package com.example.codesuongsuong

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.codesuongsuong.databinding.ItemImageBinding
import com.example.codesuongsuong.databinding.ItemTextBinding
import com.example.codesuongsuong.databinding.RvImageBinding

class Adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object{
        const val TYPE_TEXT = 1
        const val TYPE_LIST_IMAGE = 2
    }

    private  val TAG = "Adapter"
    private val mListText = ArrayList<Text>()
    @SuppressLint("NotifyDataSetChanged")
    fun addData(list: List<Text>){
        mListText.clear()
        mListText.addAll(list)
        notifyDataSetChanged()
    }

    inner class TextVH(private val binding: ItemTextBinding): RecyclerView.ViewHolder(binding.root){
        fun onBind(text: Text){
            binding.tvProduct.text = text.text
        }
    }

    inner class RvVh(private val binding: RvImageBinding): RecyclerView.ViewHolder(binding.root){
        private val imageAdapter = AdapterImage()
        fun onBind(list: List<Image>){
            binding.apply {
                rvImage.adapter = imageAdapter
                rvImage.layoutManager = LinearLayoutManager(binding.root.context,LinearLayoutManager.HORIZONTAL,false)
            }
            imageAdapter.addListImage(list)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            TYPE_TEXT-> TextVH(ItemTextBinding.inflate(LayoutInflater.from(parent.context),parent,false))
            TYPE_LIST_IMAGE -> RvVh(RvImageBinding.inflate(LayoutInflater.from(parent.context),parent,false))
            else -> TextVH(ItemTextBinding.inflate(LayoutInflater.from(parent.context),parent,false))
            /// prove
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       if (holder is TextVH){
           holder.onBind(mListText[position])
       }else if (holder is RvVh){
           val listImage = ArrayList<Image>().apply {
               add(Image(mListText[position].text))
           }
           holder.onBind(listImage)
       }
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "getItemCount: ${mListText.size}")
        return mListText.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 4){
            TYPE_LIST_IMAGE
        }else{
            TYPE_TEXT
        }
    }
}

class ItemSpanSizeLookup(private val adapter: Adapter) : GridLayoutManager.SpanSizeLookup() {
    override fun getSpanSize(position: Int): Int {
        return when(adapter.getItemViewType(position)){
            Adapter.TYPE_LIST_IMAGE-> 6
            Adapter.TYPE_TEXT -> 3
            else ->-1
        }
    }
}
