package com.example.codesuongsuong

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.codesuongsuong.databinding.ItemImageBinding

class AdapterImage: RecyclerView.Adapter<AdapterImage.ImageVh>() {
    private  val TAG = "AdapterImage"
    private val listImage = ArrayList<Image>()
    fun addListImage(listImage: List<Image>){
        this.listImage.clear()
        this.listImage.addAll(listImage)
        notifyDataSetChanged()
    }
    inner class ImageVh(private val binding: ItemImageBinding): RecyclerView.ViewHolder(binding.root){
        fun onBind(image: Image){
            Glide.with(binding.root).load(image.image).into(binding.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVh {
        return ImageVh(ItemImageBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }


    override fun getItemCount(): Int {
        Log.d(TAG, "getItemCount: ${listImage.size}")
        return listImage.size
    }

    override fun onBindViewHolder(holder: ImageVh, position: Int) {
        holder.onBind(listImage[position])
    }
}