package com.example.codesuongsuong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.codesuongsuong.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var _adapter: Adapter
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var mList: ArrayList<Text>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mList = ArrayList()
        gridLayoutManager = GridLayoutManager(this,6)
        _adapter = Adapter()
        binding.rvMain.apply {
            adapter = _adapter
            gridLayoutManager.spanSizeLookup = ItemSpanSizeLookup(_adapter)
            layoutManager = gridLayoutManager
        }
        mList = fakeData()
        for (i in 0 until mList.size){
            if (i == 4){
                mList.add(i,Text("https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"))
            }
        }
        _adapter.addData(mList)
    }

    private fun fakeData(): ArrayList<Text>{
        return ArrayList<Text>().apply {
            for (i in 0 until 20){
                add(Text(text = "Text Suong Suong ${i}"))
            }
        }
    }

}